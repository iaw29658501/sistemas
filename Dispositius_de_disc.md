
Solucions exercicis dispositius de disc

Ull viu! Vigileu i aneu amb molt de compte amb els següents exercicis

Exercici 1

Com a root executeu fdisk (de manera interactiva) per a manipular el vostre disc dur. Quina ordre heu d'executar?
```
fdisk (device) /dev/sda
```

Un cop dintre, recordem que m (man) ens mostra les diferents opcions de que disposem. Quina opció ens mostra l'arquitectura del disc dur? (És a dir, el disseny de les particions)
```
p
```
Apunteu les particions que teniu (per no eliminar-les)

```
Device     Boot     Start       End   Sectors  Size Id Type
/dev/sda1            2048 429918207 429916160  205G  5 Extended
/dev/sda5            4096 209719295 209715200  100G 83 Linux
/dev/sda6  *    209721344 419436543 209715200  100G 83 Linux
/dev/sda7       419438592 429918207  10479616    5G 83 Linux
```

Amb quina opció es pot crear una partició?
```
El comando n
```
Creeu-la (de tipus primària i de mida 1000 MB).
```
Device     Boot     Start       End   Sectors  Size Id Type
/dev/sda1            2048 429918207 429916160  205G  5 Extended
/dev/sda2       429918208 431966207   2048000 1000M 83 Linux
/dev/sda5            4096 209719295 209715200  100G 83 Linux
/dev/sda6  *    209721344 419436543 209715200  100G 83 Linux
/dev/sda7       419438592 429918207  10479616    5G 83 Linux

```

Amb quina opció es desen els canvis a la taula de particions?
```
w
```
Deseu els canvis. Llegiu el missatge de sortida i actueu en conseqüència. Ens pot servir d'ajuda esbrinar quines particions tenim disponibles, utilitzant el que hem après al tema anterior. Qualsevol ordre de les següents ens servirà:

```
ls /dev/sda*
cat /proc/partitions
ls /sys/block/sda/sda*
```

Hem de reiniciar el sistema perquè es reconegui la nova partició creada, tal i com ens informa el missatge de sortida després de fer el w del fdisk.
```
major minor  #blocks  name

   8        0  244198584 sda
   8        1          1 sda1
   8        2    1024000 sda2
   8        5  104857600 sda5
   8        6  104857600 sda6
   8        7    5239808 sda7

```
Exercici 2

Ara que ja tenim una partició la volem utilitzar. Per a això muntarem la nova partició a un directori (per exemple a /mnt/nova_particio).
```
mount /dev/sda2 /mnt/nova_particio
```

Exercici 3

Cerqueu l'executable mkfs. És a dir doneu la trajectòria on es troba el binari. Creeu el sistema de fitxers de tipus ext4 a la partició que havíeu creat abans. Intenteu ara muntar el dispositiu i crear algun fitxer a dintre.
```
mkfs -t ext4 /dev/sda2
mount /dev/sda2 /mnt/nova_particio
```

Exercici 4

Quina opció de l'ordre mount llegeix/executa les línies del fitxer /etc/fstab (a excepció de les línies que tinguin l'opció noauto)
```
mount -a
```

Exercici 5

El fitxer /etc/fstab especifica les unitats que volem muntar en l'arrencada del sistema mitjançant línies. Cadascuna d'aquestes línies està formada per sis camps. Quin d'aquest camps ens dóna les opcions de muntatge?
```
En el cuarto campo
```

Quina opció de muntatge permet muntar un dispositiu a un usuari ordinari?
```
Solo deja montar a partir de cds.
```
Si volem que un usuari qualsevol el pugui muntar i un altre, diferent d'aquest, el pugui desmuntar, quina és l'opció que ens permet fer això?
```
Solamente si este usuario esta dentro del grupo.
```
Quin camp ens indica l'ordre en que es fan els xequejos dels dispositius a muntar?
```
El sexto campo.
```
Si estic segur que un cert dispositiu no tindrà problemes(¿?), com puc fer perquè no es xequegi aquest dispositiu?
```
Igualarlo a 0.
```

Exercici 6

Atenció amb aquest perillòs exercici. Feu-lo en parelles 2 persones i 1 pc.

    Comenteu totes les línies del fitxer /etc/fstab.
    Reinicieu la màquina. Potser heu de fer un escape perquè es mostrin els missatges.
    Si després de fer aquests passos, ja no sabeu que fer demaneu la pista número 1.
    Un cop resolt aquest problema si després d'intentar restaurar el fitxer /etc/fstab no aconseguiu avançar demaneu la pista número 2.

OBS: Existeix una distribució GNU/Linux que no és per a usuaris novells però que la seva documentació és d'una gran qualitat. Sempre que busqueu algun tipus d'informació relativa a GNU/Linux tingueu-la en compte.
```
mount -o remount,rw /
ir a la ubicacion del archivo fstab
editar las linias comentadas, guardar el documento y hacer un reboot.
```
Per a la nostra pràctica ens pot ser d'ajuda el document que han fet relatiu a /etc/fstab