#!/bin/bash
# Filename: 	29.sh
# Author: 	iaw29658501
# Date: 	07/02/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 	http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	Script que coje dos argumentos los suma si el primero es menos que el segundo y los resta en casrio

# este script requiere dos argumentos 
if [ "$#" -ne 2 ]
then
	echo "Ilegal number of parameters"
	exit 1
fi
# Si el primero argumento es menos que lo segundo los sumo
if [ $1 -lt $2 ]
then
	echo $(($1 + $2)) 
# en caso contrario los resto
else
	echo $(($1 - $2))
fi

