#!/bin/bash
# Filename: 	30
# Author: 	iaw29658501
# Date: 	07/02/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 	http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	el script pide un caracter i nos dice si es una letra, numero o otra cosa

# esta variable contendra o el valor que el usuario ya has dado o lo pedira si no has dado todavia
char=$1
# este script te pedira un caracter, esto es, si no lo has dado todavia

while [ "${#char}" -ne 1 ] 
do
	echo " Please give me one character "
        read char	
done

# haora hago testes para saber que character es este

if [[ "$char" =~ ^[0-9]$ ]]
then
	echo "This character is some digit"
elif [[ "$char" =~ ^[a-zA-Z]$ ]]
then
	echo "This character is some letter"
else
	echo "This character is not a digit netter a letter, is something else"
fi      

