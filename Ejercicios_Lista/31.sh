#!/bin/bash
# Filename: 	31
# Author: 	mateosfernandez
# Date: 	14/02/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 	http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	Si el argumento es un directorio este script listara su contenido

# si el argumento es un directorio....
if [ -d $1 ]
then
	#entonses lo listo
	ls $1
else
	# si no es un directorio retorno error
	exit 1
fi

