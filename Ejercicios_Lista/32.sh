#!/bin/bash
# Filename: 	32
# Author: 	iaw29658501# Date: 	04/02/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 	http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	Suma tots el números que se li passen per paràmetre
#/bin/bash
while [ $# -ne 0 ]
do
	x=$((x+$1))
	shift
done
echo $x
