#!/bin/bash
# Filename: 	33
# Author: 	iaw29658501# Date: 	04/02/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 	http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	Compta el número de ceraràcters que tenen els noms dels fitxers del directori actual

{
for i in $(ls)
do
	echo -ne "$i\t"
	echo -n $i | wc -m 
done
} | column -t
