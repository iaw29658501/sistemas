#!/bin/bash
# Filename: 	34
# Author: 	iaw29658501
# Date: 	08/02/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 	http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	Script que determina si los numeros pasados son pares y impares

re="^[0-9]+$"
for i in $(seq $#)
do
	if [[ $1 =~ $re ]]
	then
		if [[ $1%2 -eq 0 ]]
			then
				echo "$1 es parell"
			else
				echo "$1 es senar"
		fi
	fi
	shift
done
   	
