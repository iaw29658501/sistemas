#!/bin/bash
# Filename: 	35
# Author: 	mateosfernandez
# Date: 	13/02/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 	http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	Script que pide un nombre de usuario y nos dice si este usuario existi y si existi si esta conectado
# If I get a parameter I will interprete this as the username (only viable parameter)
user=$1
#if don't get any parameters I will ask for a username
if [ $# -eq 0 ]
then
	echo "Can I get a username to search for it ?"
	read user
fi

#this is the list of users
users=$(cat /etc/passwd | cut -d ":" -f 1 )
#lets search in passwd users if this user is here

if cat /etc/passwd | cut -d ":" -f 1 | grep -qxF "$user"  
then
	echo "$user is a valid user in this system"
	#Lets check if this user that is in your system is logged now 
	if who | cut -d " " -f 1 | grep -qxF $user
	then
		echo "$user in logged"
	fi
else
	# si no encuentro el usuario retorno error 
	exit 1
fi


