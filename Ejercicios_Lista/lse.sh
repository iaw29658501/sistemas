#!/bin/bash
# Filename: 	lse
# Author: 	mateosfernandez
# Date: 	13/02/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 	http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	cript anomenat “lse” que et mostri per
#pantalla els fitxers i directoris que hi ha al directori actual
#de la seg
#ü
#ent manera:
#Si amb la comanda ls obtenim la seg
#ü
#ent sortida
#fit1   prog1   prog2   joc
#Amb el nostre script lse ens ho mostrar
#à
# aix   
# í
# :
# |_fit1
# .|_prog1
# ..|_prog2
# ...|_joc
x=0
# I will do a loop for every object in this directory to print the dots :) 
for i in $(ls -A)
do
	# lets print some dots with a loop because is the subject
	for j in $(seq $x) 
	do
		echo -n "."
	done
	# this is how we do a i++ in bash
	x=$((x+1))
	echo "|_$i"
done
