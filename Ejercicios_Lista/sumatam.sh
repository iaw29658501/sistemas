#!/bin/bash
# Filename: 	sumadir
# Author: 	mateosfernandez
# Date: 	13/02/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 	http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	Este script te dice el tamaño de los fitxeros que lo pase con error caso no exista o sea un directorio
#esta es la flag de si retorno error o no 
error="no"

#Hare un bocle para cada parametro pasado
for i in $(seq $#)
do
	# testare si el archivo existe con -e pues no me importa el tipo de fitxero desde que no lo sea un directorio
	if [ -e $1 ] && [ ! -d $1 ]
	then
	#cual es el tamaño deste archivo ?
	size=$(stat --printf="%s" $1)
	echo -e "$1\tmide\t$size\tbytes"

	#caso no sea un fitchero existente o sea un directori
	#se fuera para hacer un exit 1 no creo que deciria mensage de error 
	#entonces dare una mensage de error 
	else
		echo -e "\e[31m$1 es un directorio o no existe\e[0m" 
		error="yes"
	fi
	shift
done


# solo retorno error despues de todo lo trabajo echo pues ser un directorio o no existir no es motivo para parar 
if [ "$error" == "yes" ]
then
	exit 1
fi
