#!/bin/bash
# Filename: 	sumrest
# Author: 	iaw29658501# Date: 	15/02/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 	http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	suma o resta asi como lo pidas

function suma {
	r=$(( $1 + $2 ))
}

function resta {
	r=$(( $1 - $2 ))
}

if [ $2 == "+" ]
then
	suma $1 $3
elif [ $2 == "-" ]
then
	resta $1 $3
fi
echo $r
