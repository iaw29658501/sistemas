# /bin/bash
# author:       iaw29658501# Date:      12/04/2019
# Version:      0.2
# License:      This is free software, licensed under the GNU General Public License v3.                
# See   http://www.gnu.org/licenses/gpl.html for more information.                                      
#               Usage:  ./header.sh script_name.sh
# Description:  Validador de mac

#quitando dos puntos de la mac 
mac=$(echo $1 | sed 's/://g')
echo "MAC : $mac"
#la mac deve tener 12 characteres
if [ ${#mac} == 12 ] 
then
	#intentando converter este numero mac para un numero decimal
	printf "%d\n" 0x$mac &> /dev/null && echo "valid" || echo "invalid"
else
	echo "invalid"
fi

