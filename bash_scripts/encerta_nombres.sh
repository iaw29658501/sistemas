#!/bin/bash
# Filename:		encerta_nombres.sh
# Author:		jamoros
# Date:			30/01/2018
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./encerta_nombres.sh [arg1...]
# Description:	Un senzill joc per encertar nombres. L'usuari entra números fins que l'encerta,
# 				llavors el sistema li diu els intents fets.
# 				Si no encerta el número l'script dona una petita pista dient si és més petit o més gran.

maxim=100       # nombre màxim
intent=0        # número que proposa el jugador
num_intents=0   # intents fets pel jugador
nombre=$(( $RANDOM % $maxim + 1 ))      # número aleatori entre 1 i $maxim
echo $RANDOM
# demanem i emmagatzemem un intent
echo "Has d'endevinar en quin nombre enter estic pensant entre 1 i $maxim"
read intent

# mentre no s'encerti el número demanat, torna a demanar un número 
while [ $intent -ne $nombre ]
do
	# Informem de si el número és més gran o més petit 
	if [ $intent -lt $nombre ]
	then
		echo "el nombre és més gran que $intent"
	elif [ $intent -gt $nombre ]
	then
		echo "el nombre és més petit que $intent"
	fi
	# tornem a demanar el número i incrementem el nombre d'intents
	echo -n "Intenta -ho, de nou "
	read intent
	num_intents=$(($num_intents + 1))
done

# Sortim informant del número d'intents necessaris per encertar el número
echo "Correcte!! $nombre encertat en $num_intents intents."






































































































echo "dsskljskldjklsdjskjdhIhAUNaErrADaenaQueStscRIPt.trOBEu-lalkñlskdñlskdñlsdksd"
