#!/bin/bash
# Filename:		encerta_nombres.sh
# Author:		jamoros
# Date:			30/01/2018
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./encerta_nombres.sh [arg1...]
# Description:	Un senzill joc per encertar nombres. L'usuari entra números fins que l'encerta,
# 				llavors el sistema li diu els intents fets.
# 				Si no encerta el número l'script dona una petita pista dient si és més petit o més gran.

# nombre màxim
# número que proposa el jugador
# intents fets pel jugador
# número aleatori entre 1 i $maxim

# demanem i emmagatzemem un intent

# mentre no s'encerti el número demanat, torna a demanar un número 

	# Informem de si el número és més gran o més petit 
	
	# tornem a demanar el número i incrementem el nombre d'intents
	

# Sortim informant del número d'intents necessaris per encertar el número

