#!/bin/bash
# Filename:		match_char_string.sh
# Author:		pingui
# Date:			06/03/2018
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		match_char_string.sh cadena char
# Description:	Script que rep una cadena i un caràcter i ha de comprovar si la cadena conté el caràcter 

function is_char_in_string {

	local string=${@:1:$#-1}	# Emmagatzemmem tots els elements de l'array (@) menys l'últim
	local char=${@: -1}			# Emmagatzemmem el darrer element de l'array (@)
	
	# Mentre no arribi al final de la cadena i no trobi el caràcter char dintre vaig avançant la cadena 
	i=0
	while [ $i -lt ${#string} ] && [ "${string:$i:1}" != "$char" ]
	do
		i=$((i+1))
	done

	# Si he sortit del bucle anterior per haver arribat al final de l'string mostro que no s'ha trobat
	# altrament diem que sí s'ha trobat
	if [ $i -eq ${#string} ]
	then
		echo no està
	#	return 1  # Una altra opció és imitar una "funció booleana" amb "l'errorlevel"
	else
		echo està
	#	return 0
	fi
}

# Si no hi ha almenys dues cadenes i la segona és un únic caràcter alfabètic, sortim de l'script
if [ $# -lt 2 ]
then
	echo "error: al menys es necessiten 2 arguments: una cadena i un caràcter"
	echo "ús: $0 cadena char"
	exit 1
elif ! echo ${@: -1} | grep '^[[:alpha:]]$' >& /dev/null  # ! [[ ${@: -1} =~ ^[[:alpha:]]$ ]]
then
	echo "error: el darrer argument ha de ser un únic caràcter alfabètic"
	exit 2
fi
	
is_char_in_string $@



# Podem fer una variant: no cal imposar que el char sigui alfabètic, podem fer que tingui longitud 1
