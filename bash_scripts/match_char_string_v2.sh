#!/bin/bash
# Filename:		match_char_string_v2.sh
# Author:		pingui
# Date:			06/03/2018
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		match_char_string.sh cadena char
# Description:	Script que rep una cadena i un caràcter i ha de comprovar si la cadena conté el caràcter 

function is_char_in_string {
	
	local string=${@:1:$#-1}	# Emmagatzemmem tots els elements de l'array (@) menys l'últim
	local char=${@: -1}			# Emmagatzemmem el darrer element de l'array (@)		

	echo "string: $string"
	echo "char: $char"

	if [[ "$string" =~ $char ]]  #  echo $string | grep "$char" >& /dev/null 
	then
		echo "està"
	# podríem fer return 0 => èxit, true 
	else
		echo "no està"
	# podríem fer return 1 => no èxit, false 
	fi
}

# Si no hi ha almenys dues cadenes i la segona és un únic caràcter alfabètic, sortim de l'script

if [ $# -lt 2 ]
then
	echo "error al menys es necessiten 2 arguments: una cadena i un caràcter"
	echo "ús: $0 cadena char"
	exit 1
elif ! echo ${@: -1} | grep '^[[:alpha:]]$' >& /dev/null  # ! [[ ${@: -1} =~ ^[[:alpha:]]$ ]]
then
	echo "error el darrer argument ha de ser un únic caràcter alfabètic"
	exit 2
fi

is_char_in_string $@


# Podem fer una variant: no cal imposar que el char sigui alfabètic, podem fer que tingui longitud 1

# OBS1: Recordem que $* tracta tots els arguments com un únic string, mentre que $@ tracta els arguments com un arrai
# OBS2: Si volem fer pràctiques al bash amb els arguments $1 $2 $3 es pot fer:
#		set -- hola soc una frase
#		echo $1
#		hola
#		echo $#
#		4
#		...
