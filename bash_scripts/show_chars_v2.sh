#!/bin/bash
# Filename:		show_chars.sh
# Author:		pingui
# Date:			06/03/2018
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		show_chars.sh [arg1...]
# Description:	Script que rep una línia i la mostra separant cada caràcter en una línia. Utilitza funcions

# funció que rep una cadena i mostra un caràcter per línia per cada caràcter de la cadena

function one_char_per_line {

	# Des del 1er caràcter de la cadena fins al darrer
	# printa el caràcter i un salt de línia 	  
	local i
	for (( i = 0; i < ${#1}; i++))
	do
		echo ${1:$i:1}
	done
}

one_char_per_line "$*" # Les cometes dobles aconsegueixen que la funció només rebi un argument: *$1*
