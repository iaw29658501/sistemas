#!/bin/bash
# Filename:		swap.sh
# Author:		pingui
# Date:			16/02/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./swap.sh [arg1...]
# Description:	Script que crida a diverses funcions swap per intercanviar 2 valors fixos
# 				L'script ens mostrarà els valors abans i després de cada swap.

function swap_1 {
	local temp=${!1}
	eval $1=${!2}
	eval $2=$temp
}

function swap_2 {
	eval local temp=\$$1
	eval $1=\$$2
	eval $2=$temp
}

# Les següents dues funcions fan servir variables globals, o sigui que no serien el tipus de solucions que estem buscant

function swap_3 {
	read a b <<< "${!2} ${!1}"
}

function swap_4 {
	read a b <<< $(eval echo \$$2 \$$1)
}

a=1
b=30
echo "a=$a b=$b"
swap_1 a b
echo "a=$a b=$b"
swap_2 a b
echo "a=$a b=$b"
swap_3 a b
echo "a=$a b=$b"
swap_4 a b
echo "a=$a b=$b"


# ${!1} --->
# ${el contingut de la variable '1'}  que en aquest cas és igual a
# ${el contingut de la variable que és el primer argument de la funció} que én aquest cas és
# ${la variable a} ja que hem fet la crida a la funció "swap a b", que en aquest cas és
# el contingut de la variable a
# que en aquest cas és 10
# Més info a:
# http://stackoverflow.com/questions/540298/bash-passing-arguments-by-reference
