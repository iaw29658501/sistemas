#!/bin/bash
# Filename: 	diskmanager.sh
# Author: 	iaw29658501# Date: 	10/12/2018
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# 		See http://www.gnu.org/licenses/gpl.html for more information.
# Usage: 	./header.sh script_name.sh
# Description: 	Put the description of your program here

if [ -z $# ];then
       echo "Give me parameter for the minimal percentage of space on gandhi"
else
	echo -n "You selected to have at least $1 % of space left and have "
	space_left=$(echo "100 - $(du -sBM $HOME/$USER | cut -d M -f1)" | bc -l)
	if [ $space_left -lt $1 ]; then
		echo "NOT ENOUGHT SPACE"
	else
		echo "enoguht space"
	fi	
fi
