#!/bin/bash
# Filename: 	cron
# Author: 	iaw29658501# Date: 	24/01/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# 		See 	http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	

i=10
while [ i -gt 0 ]
do
	echo $i
done
