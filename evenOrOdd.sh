#!/bin/bash
# Filename: 	evenOrOdd.sh
# Author: 	iaw29658501# Date: 	18/01/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 		http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	Te digo si el numero es par o impar, solo me de un numero como parametro y sin puntos
if [ $# == 1 ]   
then
	valid='^[-\+]?[0-9]*$'
	if [[ "$1" =~ $valid ]]
	then
		if (( $1 % 2 ))
		then
			echo Numero impar 
		else
			echo Numero par
		fi	
	else
		echo "Esto no es un numero"
	fi
fi

