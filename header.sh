#!/bin/bash
# Filename: 	header.sh
# Author: 	iaw29658501
# Date: 	08/11/2018
# Version: 	0.3
# License: 	This is free software, licensed under the GNU General Public License v3.
# See http://www.gnu.org/licenses/gpl.html for more information.
# Usage: 	./header.sh script_name.sh
# Description:  This software adds an shell header 
#verifico si tengo parametros o si tengo de ensinar el usuario
if [ $# == 0 ]
then
	echo "usage : ./headme.sh <filename>"
	exit
fi

# si el fichero existe lo muevo temporariamente
if [ -f "$1" ]
then
	mv $1 /tmp/yourfile
else
# si el archivo no existe lo aviso
	echo "El archivo no existe"
fi
echo "Deme la descricion del script"
read D
# voy poner 
echo "#!/bin/bash" > /tmp/$1
echo -e "# Filename: \t$1" >> /tmp/$1
echo -ne "# Author: \t$(whoami)" >> /tmp/$1
echo -e "# Date: \t$(date +%d/%m/%Y)" >> /tmp/$1
echo -e "# Version: \t0.2" >> /tmp/$1
echo -e "# License: \tThis is free software, licensed under the GNU General Public License v3.\n# See \thttp://www.gnu.org/licenses/gpl.html for more information." >> /tmp/$1
echo -e "# \t\tUsage: \t./header.sh script_name.sh" >> /tmp/$1
echo -e "# Description: \t$D" >> /tmp/$1
# SI el archivo pedido ya existia

if [ -f /tmp/yourfile ]
then 
	cat /tmp/$1 | cat - /tmp/yourfile >> $1
else
	cat /tmp/$1 >> $1
fi
if [[ $1 == *.sh ]]; then
	sleep 1
else
	mv $1 $1.sh
fi

rm /tmp/yourfile
