#!/bin/bash
# Filename: 	ifumable.sh
# Author: 	iaw29658501# Date: 	28/02/2019
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 	http://www.gnu.org/licenses/gpl.html for more information.
# 		Usage: 	./header.sh script_name.sh
# Description: 	Script que te hace adivinal un numero diciendo si tu intento es mayor que no numero secreto o menor hasta que lo acerte o que pierda todos sus 100 intentos

# ESTA VARIABLE ARMAZENARA TU RESPUESTA                  
RESPUESTA=0                         
NUM_INTENTS=0                           
NOMBRE=$(( $$ % 100 ))            

# pedire por una respuesta nueva hasta que su respuesta sea igual a lo numero 
while [ $RESPUESTA -ne $NOMBRE ]
do
	echo -n "Intenta -ho? "
	read RESPUESTA
	# si la respuesta es mayor que el numero aleatorio te lo digo que asi es
	if [ $RESPUESTA -lt $NOMBRE ]
	then
		echo "... més gran!"
	# y si el menor que el numero aleatorio tambien
	elif [ $RESPUESTA -gt $NOMBRE ]
	then
		echo "... més petit!"
	fi
	NUM_INTENTS=$(($NUM_INTENTS + 1))
done
# si hadivina el numero salgo del boocle y te digo que has acertado
echo "Correcte!! $NOMBRE acertat en $NUM_INTENTS intents."
exit 0
