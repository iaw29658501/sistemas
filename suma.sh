#!/bin/bash
# Filename: 	suma.sh
# Author: 	iaw29658501# Date: 	26/11/2018
# Version: 	0.2
# License: 	This is free software, licensed under the GNU General Public License v3.
# See 	http://www.gnu.org/licenses/gpl.html for more information.
# Usage: 	./header.sh script_name.sh
# Description: 	Put the description of your program here
# Si el numero de argumentos es diferente de 2 nos mostra como se ultilizar el programa y hace un exit
if test $# -ne 2
then
	echo "Usage ./suma.sh <num1> <num2>"
	echo "Output the sum : num1 + num2"
	exit 1
fi
echo $(($1 + $2))            ### same as echo [ $1 + $2 ]
